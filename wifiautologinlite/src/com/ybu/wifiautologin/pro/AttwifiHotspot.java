package com.ybu.wifiautologin.pro;

import java.util.Collection;
import java.util.HashSet;

/**
 * ATT WIFI hot spot login method class.
 * @author michael
 */
public class AttwifiHotspot extends AbstractHotspot {
	public AttwifiHotspot(String config) {
		super(config);
	}

	public Collection<String> getSupportedSsids() {
		Collection<String> result = new HashSet<String>();
		result.add(Constants.ATTWIFI_SSID);
		result.add(Constants.WAYPORT_SSID);
		result.add(Constants.QWEST_SSID);
		return result;
	}

	@Override
	public HtmlForm processHtmlForm(HtmlForm htmlForm) {
		return htmlForm;
	}
}
