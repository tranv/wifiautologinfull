package com.ybu.wifiautologin.pro;

import java.util.Collection;

import android.content.Context;

/**
 * Interface for WIFI hot spot login. All hot spots that to be supported by this
 * program must implement this interface.
 * 
 * @author markguan
 */
public interface HotspotInterface {

	/**
	 * Gets a collection of SSIDs supported by this interface.
	 */
	public Collection<String> getSupportedSsids();

	/**
	 * Attempts to login to WIFI hot spot.
	 * 
	 * @return true if login was performed. false it you were already logged in
	 *         and no login is required.
	 * @throws Exception
	 *             if login failed.
	 */
	public boolean login(Context context) throws Exception;
}
