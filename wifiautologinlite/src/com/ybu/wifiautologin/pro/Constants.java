package com.ybu.wifiautologin.pro;

public interface Constants {
    final int HIST_LEN = 30;

    final String PREFS_NAME = "wifiautologin";
    final String PREF_KEY_ACTIVE = "active";
    final String PREF_KEY_REFRESH = "refresh";
    final String PREF_KEY_FORCE = "force";

    final String PREF_KEY_NOTIFY_WHEN_SUCCESS = "NOTIFY_WHEN_SUCCESS";
    final String PREF_KEY_NOTIFY_WHEN_ERROR = "NOTIFY_WHEN_ERROR";
    final String PREF_KEY_NOTIFY_WHEN_ALREADY_LOGGED_IN = "NOTIFY_WHEN_ALREADY_LOGGED_IN";

    // The following are supported by both pro and lite version:
    final String ATTWIFI_SSID = "attwifi";
    final String WAYPORT_SSID = "Wayport_Access";
    final String QWEST_SSID = "QwestWiFi";

    // The following are only supported by pro version:
    final String BURGERKING_SSID = "Burger King";
    final String BAGEL_NET = "BagelNet";
    final String SEATTLEBMW_SSID = "Lithia Customer Wifi";
    final String TULLYCOFFEE_SSID = "Tullys Free Wi-Fi";
    final String WORLD_WRAPPS = "World Wrapps";
    final String CANADA_STARBUCKS = "Starbucks WiFi";
    final String CANADA_MCDONALDS = "BELLWIFI@MCDONALDS";
    // final String CANSTARBUCKS_SSID = "hotspot_Bell";

    final int HOTSPOT_METHOD_1 = 1;
    final int HOTSPOT_METHOD_2 = 2;
    final int HOTSPOT_METHOD_3 = 3;
    final int HOTSPOT_METHOD_4 = 4;
    final int HOTSPOT_METHOD_5 = 5;

    final String METHOD_CONFIG = "formPattern:<form(.*?)>,"
            + "inputPattern:<input(.*?)>,"
            + "attributePattern:([\\w:\\-]+)(=(\"(.*?)\"|'(.*?)'|([^ ]*))|(\\s+|\\z))";
    final String METHOD_CONFIG_3 = "linkPattern:<a(.*?)>,"
            + "attributePattern:([\\w:\\-]+)(=(\"(.*?)\"|'(.*?)'|([^ ]*))|(\\s+|\\z))";

    final long REFRESH_INTERVAL_MS = 1000;

}
