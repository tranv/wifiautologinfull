package com.ybu.wifiautologin.pro.db;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.ybu.wifiautologin.pro.Constants;
import com.ybu.wifiautologin.pro.model.HistoryItem;
import com.ybu.wifiautologin.pro.model.LoginMethodItem;

/**
 * @author Igor Giziy <linsalion@gmail.com>
 */
public class DBAccesser {
    private static final String[] METHOD_1_SSIDS = { Constants.ATTWIFI_SSID,
            Constants.QWEST_SSID, Constants.WAYPORT_SSID };
    private static final String[] METHOD_2_SSIDS = { Constants.BURGERKING_SSID,
            Constants.BAGEL_NET, "snowy" /* For testing */};
    private static final String[] METHOD_3_SSIDS = { Constants.SEATTLEBMW_SSID,
            Constants.TULLYCOFFEE_SSID };
    private static final String[] METHOD_4_SSIDS = { Constants.CANADA_MCDONALDS };
    private static final String[] METHOD_5_SSIDS = { Constants.CANADA_STARBUCKS };

    private SQLiteDatabase db;
    private DBCreator dbCreator;

    public DBAccesser(Context context) {
        dbCreator = new DBCreator(context);
    }

    public Map<String, LoginMethodItem> getLoginMethods() {
        Map<String, LoginMethodItem> result = new HashMap<String, LoginMethodItem>();
        int counter = 0;
        for (String ssid : METHOD_1_SSIDS) {
            LoginMethodItem item = new LoginMethodItem();
            item.setId(counter++);
            item.setSsid(ssid);
            item.setMethodId(Constants.HOTSPOT_METHOD_1);
            item.setMethodConfig(Constants.METHOD_CONFIG);
            result.put(ssid, item);
        }

        // TODO(markguan): unify METHOD_1_SSIDS and METHOD_1_SSIDS process.
        for (String ssid : METHOD_2_SSIDS) {
            LoginMethodItem item = new LoginMethodItem();
            item.setId(counter++);
            item.setSsid(ssid);
            item.setMethodId(Constants.HOTSPOT_METHOD_2);
            item.setMethodConfig(Constants.METHOD_CONFIG);
            result.put(ssid, item);
        }

        for (String ssid : METHOD_3_SSIDS) {
            LoginMethodItem item = new LoginMethodItem();
            item.setId(counter++);
            item.setSsid(ssid);
            item.setMethodId(Constants.HOTSPOT_METHOD_3);
            item.setMethodConfig(Constants.METHOD_CONFIG_3);
            result.put(ssid, item);
        }

        for (String ssid : METHOD_4_SSIDS) {
            LoginMethodItem item = new LoginMethodItem();
            item.setId(counter++);
            item.setSsid(ssid);
            item.setMethodId(Constants.HOTSPOT_METHOD_4);
            item.setMethodConfig(Constants.METHOD_CONFIG);
            result.put(ssid, item);
        }

        for (String ssid : METHOD_5_SSIDS) {
            LoginMethodItem item = new LoginMethodItem();
            item.setId(counter++);
            item.setSsid(ssid);
            item.setMethodId(Constants.HOTSPOT_METHOD_5);
            item.setMethodConfig(Constants.METHOD_CONFIG);
            result.put(ssid, item);
        }

        return result;
    }

    public void addHistoryItem(HistoryItem historyItem) {
        try {
            db = dbCreator.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put("date", historyItem.getDate().getTime());
            contentValues.put("success", historyItem.isSuccess() ? 1 : 0);
            contentValues.put("message", historyItem.getMessage());
            db.insert("history", null, contentValues);
        } finally {
            db.close();
        }
    }

    public void addHistoryItems(ArrayList<HistoryItem> historyItems) {
        for (HistoryItem historyItem : historyItems) {
            addHistoryItem(historyItem);
        }
    }

    @SuppressWarnings("unused")
    private HistoryItem getHistoryItem(int id) {
        try {
            db = dbCreator.getReadableDatabase();
            Cursor cursor = db.rawQuery("select * from history where _id = "
                    + id, null);
            try {
                if (cursor.getCount() > 0) {
                    cursor.moveToFirst();
                    return readHistoryItem(cursor);
                } else
                    return null;
            } finally {
                cursor.close();
            }
        } finally {
            db.close();
        }
    }

    private HistoryItem readHistoryItem(Cursor cursor) {
        HistoryItem historyItem = new HistoryItem();
        historyItem.setId(cursor.getInt(0));
        historyItem.setDate(new Date(cursor.getLong(1)));
        historyItem.setSuccess(cursor.getInt(2) != 0);
        historyItem.setMessage(cursor.getString(3));
        return historyItem;
    }

    public ArrayList<HistoryItem> getHistoryItems(int n) {
        ArrayList<HistoryItem> historyItems = new ArrayList<HistoryItem>();
        try {
            db = dbCreator.getReadableDatabase();
            Cursor cursor = db
                    .rawQuery("select * from history order by date desc limit "
                            + n + ";", null);
            try {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    historyItems.add(readHistoryItem(cursor));
                    cursor.moveToNext();
                }
                return historyItems;
            } finally {
                cursor.close();
            }
        } finally {
            db.close();
        }
    }

    public void removeHistoryItem(int id) {
        try {
            db = dbCreator.getWritableDatabase();
            db.delete("history", "_id = " + id, null);
        } finally {
            db.close();
        }
    }

    public void removeHistoryItems() {
        try {
            db = dbCreator.getWritableDatabase();
            db.delete("history", null, null);
        } finally {
            db.close();
        }

    }

    public int getMaxId() {
        try {
            db = dbCreator.getReadableDatabase();
            Cursor cursor = db.rawQuery("SELECT max(_id) from history", null);
            try {
                if (cursor.getCount() > 0) {
                    cursor.moveToFirst();
                    return cursor.getInt(0);
                } else {
                    return 0;
                }
            } finally {
                cursor.close();
            }
        } finally {
            if (db != null)
                db.close();
        }
    }
}
