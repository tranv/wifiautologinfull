package com.ybu.wifiautologin.pro;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;

/**
 * Burger King hot spot login method class.
 *
 * @author markguan
 */
public class BurgerKingHotspot extends AbstractHotspot {
    public BurgerKingHotspot(String config) {
        super(config);
    }

    public Collection<String> getSupportedSsids() {
        Collection<String> result = new HashSet<String>();
        result.add(Constants.BURGERKING_SSID);
        result.add(Constants.BAGEL_NET);
        result.add(Constants.WORLD_WRAPPS);
        return result;
    }

    @Override
    public HtmlForm processHtmlForm(HtmlForm htmlForm) {
        Map<String, String> parameters = htmlForm.getParameters();
        parameters.put("answer", "1");
        htmlForm.setParameters(parameters);
        return htmlForm;
    }
}
