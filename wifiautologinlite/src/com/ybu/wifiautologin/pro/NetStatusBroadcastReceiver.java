package com.ybu.wifiautologin.pro;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.Map;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.Log;

import com.ybu.wifiautologin.pro.db.DBAccesser;
import com.ybu.wifiautologin.pro.model.HistoryItem;
import com.ybu.wifiautologin.pro.model.LoginMethodItem;
import com.ybu.wifiautologin.pro.R;

public class NetStatusBroadcastReceiver extends BroadcastReceiver {
	private static final String TAG = "WifiAutoLogin";
	private Context context;

	@Override
	public void onReceive(Context context, Intent intent) {
		this.context = context;

		final String action = intent.getAction();
		Log.d(TAG, "Broadcast received. Action=" + action);

		SharedPreferences settings = context.getSharedPreferences(
				Constants.PREFS_NAME, 0);
		if (!settings.getBoolean(Constants.PREF_KEY_ACTIVE, true)) {
			Log.i(TAG, "Disabled. Ignoring broadcast.");
			return;
		}

		// check if device is connected to any network
		NetworkInfo ni = (NetworkInfo) intent
				.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
		if (ni == null || !ni.isConnected()) {
			Log.d(TAG, "Not connected");
			return;
		}

		// check the current connected wifi network
		WifiManager wifi = (WifiManager) context
				.getSystemService(Context.WIFI_SERVICE);

		WifiInfo winfo = wifi.getConnectionInfo();
		String ssid = winfo.getSSID();
		// Android 4.2 put double quote " around the wifi name.
		// Now we need to remove them.
		if (ssid != null && ssid.startsWith("\"") && ssid.endsWith("\"")) {
			ssid = ssid.substring(1, ssid.length() - 1);
		}
		DBAccesser db = new DBAccesser(context);

		LoginMethodItem loginMethod = getLoginMethod(db, ssid);
		if (loginMethod == null) {
			Log.d(TAG, "Unknown SSID " + ssid);
			return;
		}

		HotspotInterface s = getLoginClass(loginMethod);
		if (s == null) {
			Log.d(TAG, "Unknown login method id " + loginMethod.getMethodId());
			return;
		}

		SharedPreferences prefs = context.getSharedPreferences(
				Constants.PREFS_NAME, Context.MODE_PRIVATE);
		HistoryItem h = new HistoryItem();
		h.setDate(new Date());
		Log.d(TAG, "Hotspot SSID detected. SSID = " + ssid);

		try {
			boolean status = s.login(context);
			h.setSuccess(true);
			if (status) {
				if (prefs.getBoolean(Constants.PREF_KEY_NOTIFY_WHEN_SUCCESS,
						true)) {
					createNotification(context
							.getString(R.string.notify_message_success));
				}
				h.setMessage("Connected to " + ssid);
			} else {
				if (prefs
						.getBoolean(
								Constants.PREF_KEY_NOTIFY_WHEN_ALREADY_LOGGED_IN,
								false)) {
					createNotification(context
							.getString(R.string.notify_message_already_logged));
				}
				h.setMessage("Connected to " + ssid);
			}
		} catch (Exception e) {

			try {
				URL url = new URL("http://wifiautologin.com/ping/");
				HttpURLConnection conn = (HttpURLConnection) url
						.openConnection();
				StringBuilder page = Toolbox.getHttpContent(conn);
				boolean isRealPage = page.toString().contains("pong");
				if (!isRealPage) {
					h.setSuccess(false);
					h.setMessage("Login to " + ssid + " failed: "
							+ e.getMessage());
					if (prefs.getBoolean(Constants.PREF_KEY_NOTIFY_WHEN_ERROR,
							true)) {
						createNotification(context
								.getString(R.string.notify_message_error));
					}
					Log.e(TAG, "Login failed", e);
				} else {
					if (prefs.getBoolean(
							Constants.PREF_KEY_NOTIFY_WHEN_SUCCESS, true)) {
						createNotification(context
								.getString(R.string.notify_message_success));
					}
					h.setSuccess(true);
					h.setMessage("Connected to " + ssid + ".");
				}
				conn.disconnect();
			} catch (MalformedURLException e1) {
				e1.printStackTrace();
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		db.addHistoryItem(h);
		// add separator
		h.setMessage("sep");
		db.addHistoryItem(h);
	}

	private LoginMethodItem getLoginMethod(DBAccesser db, String ssid) {
		Map<String, LoginMethodItem> loginMethods = db.getLoginMethods();
		return loginMethods.get(ssid);
	}

	private HotspotInterface getLoginClass(LoginMethodItem item) {
		if (item.getMethodId() == Constants.HOTSPOT_METHOD_1) {
			return new AttwifiHotspot(item.getMethodConfig());
		} else if (item.getMethodId() == Constants.HOTSPOT_METHOD_2) {
			return new BurgerKingHotspot(item.getMethodConfig());
		} else if (item.getMethodId() == Constants.HOTSPOT_METHOD_3) {
			return new LinkHotspot(item.getMethodConfig());
		} else if (item.getMethodId() == Constants.HOTSPOT_METHOD_4) {
			return new CanadaMcDonaldsHotspot(item.getMethodConfig());
		} else if (item.getMethodId() == Constants.HOTSPOT_METHOD_5) {
			return new CanadaStarbucksHotspot(item.getMethodConfig());
		} else {
			return null;
		}
	}

	private void createNotification(String message) {
		NotificationManager notificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		Notification notification = new Notification(
				R.drawable.wifiautologin32,
				context.getString(R.string.notify_title),
				System.currentTimeMillis());
		Intent notificationIntent = new Intent(context, MainActivity.class);
		PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
				notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
		notification.setLatestEventInfo(context,
				context.getString(R.string.notify_title), message,
				contentIntent);
		notification.flags = Notification.FLAG_AUTO_CANCEL;
		notificationManager.notify(0, notification);
	}
}
