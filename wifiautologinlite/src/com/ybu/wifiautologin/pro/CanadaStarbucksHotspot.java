package com.ybu.wifiautologin.pro;

import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;

import android.content.Context;
import android.util.Log;

import com.ybu.wifiautologin.pro.db.DBAccesser;
import com.ybu.wifiautologin.pro.model.HistoryItem;

/**
 * Starbucks in Canada hot spot login method class.
 * 
 * @author markguan
 */
public class CanadaStarbucksHotspot extends AbstractHotspot {
    private static final String TEST_URL = "http://wifiautologin.com/ping/";
    private static final String TAG = "WifiAutoLogin";
    private static final String USER_AGENT = "Mozilla/5.0 (Linux; U; " +
    		"Android 4.0.3; ko-kr; LG-L160L Build/IML74K) AppleWebkit/534.30 " +
    		"(KHTML, like Gecko) Version/4.0 Mobile Safari/534.30";

	public CanadaStarbucksHotspot(String config) {
		super(config);
	}

	public Collection<String> getSupportedSsids() {
		Collection<String> result = new HashSet<String>();
		result.add(Constants.CANADA_STARBUCKS);
		return result;
	}

	@Override
	public HtmlForm processHtmlForm(HtmlForm htmlForm) {
		Map<String, String> parameters = htmlForm.getParameters();
		parameters.put("chkTerms", "on");
		htmlForm.setParameters(parameters);
		return htmlForm;
	}
	
	@Override
    public boolean login(Context context) throws Exception {
        URL testURL = new URL(TEST_URL);

        // disable the automatic following of redirects
        // a 3xx response can be used to determine whether or not the computer
        // is already connected to the Internet
        HttpURLConnection.setFollowRedirects(false);

        // try to visit a website
        Log.d(TAG, "Attempting to visit [" + testURL + "]...");

        // added
        DBAccesser db = new DBAccesser(context);
        HistoryItem h = new HistoryItem();
        h.setDate(new Date());
        h.setMessage("Connecting to Internet ...");
        h.setSuccess(true);
        db.addHistoryItem(h);

        HttpURLConnection conn = (HttpURLConnection) testURL.openConnection();
        conn.setRequestProperty("User-Agent", USER_AGENT);
        conn.setDoInput(true);
        conn.setDoOutput(false);
        conn.setRequestMethod("GET");
        int responseCode = conn.getResponseCode();

        StringBuilder page = Toolbox.getHttpContent(conn);
        h.setMessage("initial HTML=" + page.toString());
        db.addHistoryItem(h);

        boolean isRealPage = page.toString().contains("pong");

        if (responseCode == HttpURLConnection.HTTP_MOVED_TEMP || !isRealPage) {
            // if you haven't accepted the terms and conditions yet, 302 is
            // returned, redirecting you to the login page

            h.setSuccess(true);
            h.setMessage("Trying to log in.");
            db.addHistoryItem(h);

            // get the Location header, which contains the redirect URL
            String redirectUrlStr = conn.getHeaderField("Location");

            if ((redirectUrlStr == null || redirectUrlStr.trim().length() == 0) && !isRealPage) {
                redirectUrlStr = TEST_URL;
            }

            // go to the redirect URL, which is the Hotspot login page
            conn.disconnect();
            URL redirectUrl = new URL(redirectUrlStr);
            Log.d(TAG, "Downloading Hotspot login page [" + redirectUrl
                    + "]...");
            conn = (HttpURLConnection) redirectUrl.openConnection();
            conn.setRequestProperty("User-Agent", USER_AGENT);
            conn.setDoInput(true);
            conn.setDoOutput(false);
            conn.setRequestMethod("GET");

            String cookieString = null;
            if (handleCookie()) {
            	cookieString = conn.getHeaderField("Set-Cookie");
            }

            StringBuilder html = Toolbox.getHttpContent(conn);
            conn.disconnect();
            
            h.setMessage("login HTML=" + html.toString());
            db.addHistoryItem(h);
            
            //===================================================
            // Starbucks used meta refresh tag to do redirect. 
            // So we handle it here
            String refreshURLStr = getRefreshUrl(html.toString());
            h.setMessage("refresh URL=" + refreshURLStr);
            db.addHistoryItem(h);

            URL refreshURL = new URL(refreshURLStr);
            conn = (HttpURLConnection) refreshURL.openConnection();
            conn.setRequestProperty("User-Agent", USER_AGENT);
            conn.setDoInput(true);
            conn.setDoOutput(false);
            conn.setRequestMethod("GET");

            html = Toolbox.getHttpContent(conn);
            conn.disconnect();
            
            h.setMessage("refresh HTML=" + html.toString());
            db.addHistoryItem(h);
            //===================================================
            
            
            // parse the form info out of the HTML
            Log.d(TAG, "Parsing Hotspot login page...");
            HtmlForm formInfo = new HtmlForm(getMethodConfig(), redirectUrl,
                    html.toString());

            formInfo = processHtmlForm(formInfo);

            // prepare to submit the form
            Log.d(TAG, "Accepting the terms and conditions...");
            conn = (HttpURLConnection) formInfo.getActionUrl().openConnection();
            conn.setRequestProperty("User-Agent", USER_AGENT);
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestMethod(formInfo.getMethod());
            if (handleCookie() && cookieString != null) {
                conn.setRequestProperty("Cookie", cookieString);
            }

            // output parameters to request body
            StringBuilder sb = new StringBuilder();
            for (Map.Entry<String, String> entry : formInfo.getParameters()
                    .entrySet()) {
                sb.append(URLEncoder.encode(entry.getKey(), "UTF-8") + '='
                        + URLEncoder.encode(entry.getValue(), "UTF-8") + '&');
            }
            PrintWriter out = new PrintWriter(conn.getOutputStream());
            out.print(sb.substring(0, sb.length() - 1)); // remove the last '&'
            out.flush();

            // send request
            conn.getResponseCode();
            conn.disconnect();

            // try to connect to the Internet again to see if it worked
            conn = (HttpURLConnection) testURL.openConnection();
            conn.setRequestProperty("User-Agent", USER_AGENT);
            conn.setDoInput(true);
            conn.setDoOutput(false);
            conn.setRequestMethod("GET");
            responseCode = conn.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                Log.d(TAG,
                        "SUCCESS: The terms and conditions have been agreed to and you can now connect to the Internet!");
                return (true);
            } else {
                Log.e(TAG,
                        "Error: Approval of terms and conditions failed. HTTP status code "
                                + responseCode);
                throw new Exception(
                        "Error: Approval of terms and conditions failed. HTTP status code "
                                + responseCode);
            }
        } else if (responseCode == HttpURLConnection.HTTP_OK) {
            Log.d(TAG, "Re-connecting to prior connection.");
            return (false);
        } else {
            Log.e(TAG, "Unknown error: HTTP status code " + responseCode);
            throw new Exception("Unknown error: HTTP status code "
                    + responseCode);
        }
    }

	private String getRefreshUrl(String html) {
		String metaTag = "meta http-equiv=\"refresh\" content=\"5;url=";
		int beg = html.indexOf(metaTag);
		beg += metaTag.length();
		int end = html.indexOf("\"", beg);
		
		return html.substring(beg, end);
	}
}
