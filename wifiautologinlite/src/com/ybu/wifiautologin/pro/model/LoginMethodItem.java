package com.ybu.wifiautologin.pro.model;

/**
 * @author Mark Guan <markguan.android@gmail.com>
 */
public class LoginMethodItem {
	private int id;
	private String ssid;
	private int methodId;
	private String methodConfig;

	public String getSsid() {
		return ssid;
	}

	public void setSsid(String ssid) {
		this.ssid = ssid;
	}

	public int getMethodId() {
		return methodId;
	}

	public void setMethodId(int methodId) {
		this.methodId = methodId;
	}

	public String getMethodConfig() {
		return methodConfig;
	}

	public void setMethodConfig(String methodConfig) {
		this.methodConfig = methodConfig;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String toString() {
		return String.format("id:%d, ssid:%s, method_id:%d, method_config:%s",
				id, ssid, methodId, methodConfig);
	}
}
