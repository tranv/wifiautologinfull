package com.ybu.wifiautologin.pro;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;

/**
 * McDonalds in Canada hot spot login method class.
 * 
 * @author markguan
 */
public class CanadaMcDonaldsHotspot extends AbstractHotspot {
	public CanadaMcDonaldsHotspot(String config) {
		super(config);
	}

	@Override
	public Collection<String> getSupportedSsids() {
		Collection<String> result = new HashSet<String>();
		result.add(Constants.CANADA_MCDONALDS);
		return result;
	}

	@Override
    public boolean handleCookie() {
		return true;
	}
	
	@Override
	public HtmlForm processHtmlForm(HtmlForm htmlForm) {
		Map<String, String> parameters = htmlForm.getParameters();
		parameters.put("termsChecked", "on");
		htmlForm.setParameters(parameters);
		return htmlForm;
	}
}
