package com.ybu.wifiautologin.pro;

import android.app.Activity;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.ybu.wifiautologin.pro.R;

public class About extends Activity {

	private final String aboutStr = "<p>WiFi Auto-Login PRO AUTOMATICALLY logs in your Android "
			+ "device into some of the most popular FREE WiFi hotspots, eliminating the need to "
			+ "deal with that annoying WiFi hotspot login page! WiFi Auto-Login PRO supports more "
			+ "WiFi hotspots than the &quot;Lite&quot; version.</p> <p><strong>MORE Hotspots: "
			+ "</strong>WiFi Auto-Login PRO supports Burger King, Noah Bagel, "
			+ "Einstein Bros Bagels, in addition to Starbucks, McDonald's, Barnes &amp; Noble, "
			+ "Coffee Bean, Tea Leaf, FedEx Office (aka Kinko's) that are supported in the "
			+ "&quot;Lite&quot; version.</p>";

    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);// hide title bar
        setContentView(R.layout.about);

        TextView url = (TextView) findViewById(R.id.url);
        url.setText(Html.fromHtml("<a href='http://www.ybumobile.com/'>http://www.ybumobile.com/</a>"));
        url.setMovementMethod(LinkMovementMethod.getInstance());

        TextView email = (TextView) findViewById(R.id.email);
        email.setText(Html.fromHtml("<a href='mailto:wifiautologin@ybumobile.com'>wifiautologin@ybumobile.com</a>"));
        email.setMovementMethod(LinkMovementMethod.getInstance());

        TextView about = (TextView) findViewById(R.id.about);
        about.setText(Html.fromHtml(aboutStr));

        Button okBtn = (Button) findViewById(R.id.okBtn);
        okBtn.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                finish();
            }
        });
    }
}
