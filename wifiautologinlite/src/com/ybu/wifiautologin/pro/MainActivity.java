package com.ybu.wifiautologin.pro;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.ybu.wifiautologin.pro.db.DBAccesser;
import com.ybu.wifiautologin.pro.model.HistoryItem;
import com.ybu.wifiautologin.pro.R;

public class MainActivity extends Activity {
    private static final String TAG = "WiFiAutoLogin";
    private boolean update = true;
    private Object monitor = new Object();
    private boolean needRetry = false;
    private TextView statusTxt;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);// hide title bar
        setContentView(R.layout.main);

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(WifiManager.SUPPLICANT_STATE_CHANGED_ACTION);
        intentFilter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
        intentFilter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);

        this.registerReceiver(wifiStateBroadcastReceiver, intentFilter);

        ToggleButton activeToggle = (ToggleButton) findViewById(R.id.active);
        Button clearBtn = (Button) findViewById(R.id.clearBtn);
        ImageView infoBtn = (ImageView) findViewById(R.id.infoBtn);
        SharedPreferences settings = getSharedPreferences(Constants.PREFS_NAME,
                0);
        activeToggle.setChecked(settings.getBoolean(Constants.PREF_KEY_ACTIVE,
                true));
        statusTxt = (TextView) findViewById(R.id.status);

        activeToggle.setOnClickListener(new OnClickListener() {
            public void onClick(View buttonView) {
                SharedPreferences settings = getSharedPreferences(
                        Constants.PREFS_NAME, 0);
                SharedPreferences.Editor editor = settings.edit();
                editor.putBoolean(Constants.PREF_KEY_ACTIVE,
                        ((ToggleButton) buttonView).isChecked());
                editor.commit();
            }
        });

        clearBtn.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                // TODO Auto-generated method stub
                Log.d(TAG, "Clearing history");
                DBAccesser db = new DBAccesser(MainActivity.this);
                db.removeHistoryItems();
                synchronized (monitor) {
                    monitor.notify();
                }
            }
        });

        infoBtn.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(MainActivity.this, About.class);
                startActivity(intent);
            }
        });

    }

    private BroadcastReceiver wifiStateBroadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            int extraWifiState = intent.getIntExtra(
                    WifiManager.EXTRA_WIFI_STATE,
                    WifiManager.WIFI_STATE_UNKNOWN);

            Log.d("wifiautologinv", intent.getAction());

            if (intent.getAction().equals(
                    "android.net.wifi.supplicant.STATE_CHANGE")) {
                getStatus();
            }

            switch (extraWifiState) {
            case WifiManager.WIFI_STATE_DISABLED:
                statusTxt.setText("Status: Wifi disabled!");
                statusTxt.setTextColor(Color.RED);
            case WifiManager.WIFI_STATE_DISABLING:
                break;
            case WifiManager.WIFI_STATE_ENABLED:
                getStatus();
                break;
            case WifiManager.WIFI_STATE_ENABLING:
                break;
            case WifiManager.WIFI_STATE_UNKNOWN:
                break;
            }
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        update = true;
        startUpdateThread();
    }

    @Override
    public void onPause() {
        super.onPause();
        synchronized (monitor) {
            update = false;
            monitor.notify();
        }
    }

    public void startUpdateThread() {
        new Thread(new Runnable() {
            public void run() {
                DBAccesser db = new DBAccesser(MainActivity.this);
                handler.sendEmptyMessage(0);
                long maxId = db.getMaxId();
                while (true) {
                    int newMaxId = db.getMaxId();
                    if (maxId < newMaxId || newMaxId == 0) {
                        handler.sendEmptyMessage(0);
                        maxId = newMaxId;
                    } else if (needRetry) {
                        needRetry = false;
                        handler.sendEmptyMessage(0);
                    }

                    try {
                        synchronized (monitor) {
                            monitor.wait(Constants.REFRESH_INTERVAL_MS);
                            if (!update)
                                break;
                        }
                    } catch (InterruptedException e) {
                    }
                }
            }
        }).start();
    }

    public Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            getStatus();
            showHistory();
        }
    };

    private void showHistory() {
        DBAccesser db = new DBAccesser(this);
        ArrayList<HistoryItem> hist;
        try {
            hist = db.getHistoryItems(Constants.HIST_LEN);
        } catch (Exception e) {
            needRetry = true;
            return;
        }

        TableLayout histtable = (TableLayout) findViewById(R.id.histTable);
        histtable.setStretchAllColumns(true);
        histtable.setShrinkAllColumns(true);
        histtable.removeAllViews();
        if (hist.isEmpty()) {
            TextView nohist = new TextView(this);
            nohist.setText(R.string.nohist);
            nohist.setTypeface(Typeface.DEFAULT, Typeface.ITALIC);
            histtable.addView(nohist);
        } else {

            int i = 0;
            for (HistoryItem h : hist) {
                i++;

                if (h == null || h.getMessage() == null) {
                	continue;
                }
                
                if (h.getMessage().equalsIgnoreCase("sep")) {
                    TextView sep = new TextView(this);
                    sep.setBackgroundColor(Color.GRAY);
                    sep.setHeight(1);
                    histtable.addView(sep);
                    continue;
                }

                // create layout for a row
                RelativeLayout row = new RelativeLayout(this);
                row.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,
                        LayoutParams.FILL_PARENT));

                // prepare suitable icon
                ImageView icon = new ImageView(this);
                icon.setImageResource(h.isSuccess() ? android.R.drawable.presence_online
                        : android.R.drawable.presence_busy);
                LayoutParams params = new LayoutParams(
                        LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
                icon.setLayoutParams(params);
                icon.setId(1019 + i * 3);
                row.addView(icon);

                // prepare date cell
                TextView dateCell = new TextView(this);
                CharSequence ds = DateUtils.formatSameDayTime(h.getDate()
                        .getTime(), new Date().getTime(), DateFormat.SHORT,
                        DateFormat.SHORT);
                dateCell.setText(ds);
                params = new LayoutParams(LayoutParams.WRAP_CONTENT,
                        LayoutParams.WRAP_CONTENT);
                params.addRule(RelativeLayout.RIGHT_OF, 1019 + i * 3);
                dateCell.setLayoutParams(params);
                dateCell.setId(1020 + i * 3);
                row.addView(dateCell);

                // prepare text message
                TextView msgCell = new TextView(this);
                msgCell.setText(h.getMessage());
                params = new LayoutParams(LayoutParams.WRAP_CONTENT,
                        LayoutParams.WRAP_CONTENT);
                params.addRule(RelativeLayout.RIGHT_OF, 1020 + i * 3);
                params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
                msgCell.setLayoutParams(params);
                msgCell.setPadding(10, 0, 0, 0);
                row.addView(msgCell);

                histtable.addView(row);
            }
        }
    }

    @SuppressWarnings("unused")
    private void addTestData() {
        HistoryItem h = new HistoryItem();

        DBAccesser db = new DBAccesser(this);
        db.removeHistoryItems();
        for (int i = 0; i < 2; i++) {
            h.setDate(new Date());
            h.setSuccess(i % 2 == 0);
            h.setMessage("Attempt longgggg ggggggggggg gggggggggggggggggggggggg gggggggggggggggggggggggggggggggggg"
                    + i);
            db.addHistoryItem(h);
        }
    }

    private void getStatus() {
        WifiManager wifiMan = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiMan.getConnectionInfo();
        String ssid = wifiInfo.getSSID();
        if (ssid != null && !ssid.equals("<unknown ssid>")) {
            statusTxt.setTextColor(Color.YELLOW);
            statusTxt.setText("Status: Currently connected to " + ssid);
        } else {
        	statusTxt.setTextColor(Color.RED);
            statusTxt.setText("Status: No network Connected!");
        }
    }

    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        try {
            this.unregisterReceiver(wifiStateBroadcastReceiver);
            Log.d(TAG, "Service Unregistered");
        } catch (IllegalArgumentException e) {
            Log.d(TAG, e.getMessage());
        }
    }

}