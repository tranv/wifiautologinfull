package com.ybu.wifiautologin.pro;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;

public class Toolbox {

	public static StringBuilder getHttpContent(HttpURLConnection conn)
			throws IOException {
		// get the HTML of the webpage
		BufferedReader in = new BufferedReader(new InputStreamReader(
				conn.getInputStream()));
		String line;
		StringBuilder html = new StringBuilder();
		while ((line = in.readLine()) != null) {
			html.append(line);
		}
		in.close();
		return html;
	}
}
