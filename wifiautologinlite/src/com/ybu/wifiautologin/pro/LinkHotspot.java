package com.ybu.wifiautologin.pro;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.content.Context;
import android.util.Log;

import com.ybu.wifiautologin.pro.db.DBAccesser;
import com.ybu.wifiautologin.pro.model.HistoryItem;

public class LinkHotspot implements HotspotInterface {
	private static final String TEST_URL = "http://wifiautologin.com/ping/";
	private static final String TAG = "WifiAutoLogin";
	private final String methodConfig;

	public LinkHotspot(String config) {
		methodConfig = config;
	}

	public Collection<String> getSupportedSsids() {
		Collection<String> result = new HashSet<String>();
		result.add(Constants.SEATTLEBMW_SSID);
		result.add(Constants.TULLYCOFFEE_SSID);

		result.add("NoInternet");
		return result;
	}

	public boolean login(Context context) throws Exception {
		URL testURL = new URL(TEST_URL);

		// disable the automatic following of redirects
		// a 3xx response can be used to determine whether or not the computer
		// is already connected to the Internet
		HttpURLConnection.setFollowRedirects(false);

		// try to visit a website
		Log.d(TAG, "Attempting to visit [" + testURL + "]...");

		// added
		DBAccesser db = new DBAccesser(context);
		HistoryItem h = new HistoryItem();
		h.setDate(new Date());
		h.setMessage("Connecting to Internet ...");
		h.setSuccess(true);
		db.addHistoryItem(h);

		HttpURLConnection conn = (HttpURLConnection) testURL.openConnection();

		conn.setDoInput(true);
		conn.setDoOutput(false);
		conn.setRequestMethod("GET");
		int responseCode = conn.getResponseCode();

		StringBuilder page = Toolbox.getHttpContent(conn);
		boolean isRealPage = page.toString().contains("pong");

		if (responseCode == HttpURLConnection.HTTP_MOVED_TEMP || !isRealPage) {
			// if you haven't accepted the terms and conditions yet, 302 is
			// returned, redirecting you to the login page

			h.setSuccess(true);
			h.setMessage("Trying to log in.");

			db.addHistoryItem(h);

			// get the Location header, which contains the redirect URL
			String redirectUrlStr = conn.getHeaderField("Location");

			if (redirectUrlStr == null || !isRealPage) {
				redirectUrlStr = TEST_URL;
			}

			// go to the redirect URL, which is the Hotspot login page
			conn.disconnect();
			URL redirectUrl = new URL(redirectUrlStr);
			Log.d(TAG, "Downloading Hotspot login page [" + redirectUrl
					+ "]...");
			conn = (HttpURLConnection) redirectUrl.openConnection();
			conn.setDoInput(true);
			conn.setDoOutput(false);
			conn.setRequestMethod("GET");

			StringBuilder html = Toolbox.getHttpContent(conn);
			conn.disconnect();

			// get continue link
			String continueLink = getContinueLink(html.toString());
			if (continueLink != null) {
				h.setSuccess(true);
				h.setMessage("Continue Link found :" + continueLink);
				db.addHistoryItem(h);
				URL continueURL = new URL(continueLink);
				conn = (HttpURLConnection) continueURL.openConnection();
				conn.setDoInput(true);
				conn.setDoOutput(false);
				conn.setRequestMethod("GET");
			}

			// send request
			conn.getResponseCode();
			conn.disconnect();

			// try to connect to the Internet again to see if it worked
			conn = (HttpURLConnection) testURL.openConnection();
			conn.setDoInput(true);
			conn.setDoOutput(false);
			conn.setRequestMethod("GET");
			responseCode = conn.getResponseCode();
			if (responseCode == HttpURLConnection.HTTP_OK) {
				Log.d(TAG,
						"SUCCESS: The terms and conditions have been agreed to and you can now connect to the Internet!");
				return (true);
			} else {
				Log.e(TAG,
						"Error: Approval of terms and conditions failed. HTTP status code "
								+ responseCode);
				throw new Exception(
						"Error: Approval of terms and conditions failed. HTTP status code "
								+ responseCode);
			}
		} else if (responseCode == HttpURLConnection.HTTP_OK) {
			Log.d(TAG, "Re-connecting to prior connection.");
			return (false);
		} else {
			Log.e(TAG, "Unknown error: HTTP status code " + responseCode);
			throw new Exception("Unknown error: HTTP status code "
					+ responseCode);
		}
	}

	private String getContinueLink(String html) {
		String continueLink = null;

		String[] configStrs = methodConfig.split(",");

		String link = configStrs[0].substring("linkPattern:".length());
		String attribute = configStrs[1]
				.substring("attributePattern:".length());

		Pattern linkPattern = Pattern.compile(link, Pattern.CASE_INSENSITIVE
				| Pattern.DOTALL);
		Pattern attributePattern = Pattern.compile(attribute);

		Matcher matcher = linkPattern.matcher(html);

		while (matcher.find()) {
			Map<String, String> attributes = parseAttributes(attributePattern,
					matcher.group(1));
			Log.d(TAG, "aps " + attributes);
			if (attributes.containsKey("id")
					&& attributes.get("id").equalsIgnoreCase("continue_link")) {
				continueLink = attributes.get("href");
			}

		}

		return continueLink;
	}

	private Map<String, String> parseAttributes(Pattern attributePattern,
			String attributesStr) {
		Map<String, String> attributes = new HashMap<String, String>();

		Matcher matcher = attributePattern.matcher(attributesStr);
		while (matcher.find()) {
			String key = matcher.group(1);
			String value = null;
			if (matcher.group(2).trim().length() == 0) {
				// it's an attribute with no value
				value = "";
			} else {
				for (int i = 4; i <= 6; i++) {
					String g = matcher.group(i);
					if (g != null) {
						value = g;
						break;
					}
				}
			}
			attributes.put(key, value.trim());
		}

		return attributes;
	}

}
